import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {
RouterModule,
Routes
} from '@angular/router';

import { AppComponent } from './app.component';
import { AssetMenuComponent } from './asset-menu/asset-menu.component';
import { AssetListComponent } from './asset-list/asset-list.component';
import { AssetNavigatorComponent } from './asset-navigator/asset-navigator.component';
import { AssetNavItemComponent } from './asset-nav-item/asset-nav-item.component';
import { CategoryNavItemComponent } from './category-nav-item/category-nav-item.component';
import { OwnerNavItemComponent } from './owner-nav-item/owner-nav-item.component';
import { AssetEditorComponent } from './asset-editor/asset-editor.component';
import { OwnerListComponent } from './owner-list/owner-list.component';
import { CategoryListComponent } from './category-list/category-list.component';
import { HomeComponent } from './home/home.component';
import { CategoryEditorComponent } from './category-editor/category-editor.component';
import { OwnerEditorComponent } from './owner-editor/owner-editor.component';

 const routes: Routes = [
  // basic routes
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'assets', component: AssetListComponent },
  { path: 'categories', component: CategoryListComponent },
  { path: 'owners', component: OwnerListComponent }
  // authentication demo
  // { path: 'login', component: LoginComponent },
  // {
  // path: 'protected',
  // component: ProtectedComponent,
  // canActivate: [ LoggedInGuard ]
  // },

  // nested
  // {
  // path: 'products',
  // component: ProductsComponent,
  // children: childRoutes
  // }
];

@NgModule({
  declarations: [
    AppComponent,
    AssetMenuComponent,
    AssetListComponent,
    AssetNavigatorComponent,
    AssetNavItemComponent,
    CategoryNavItemComponent,
    OwnerNavItemComponent,
    AssetEditorComponent,
    OwnerListComponent,
    CategoryListComponent,
    HomeComponent,
    CategoryEditorComponent,
    OwnerEditorComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
