/**
* Provides a `Asset` object
*/

export class Owner{
	constructor(
		public id: number,
		public name: string,
		public last_name: string
	){}
}

export class Category{
	constructor(
		public id: number,
		public name: string
	){}
	public getId()
	{
		return this.id;
	}
}

export class Asset {
	

	constructor(
		public id: number,
		public price: number,
		public epc_id: string,
		public last_reading: string,
		public status: string, 
		public owner: number,
		public category: number)
	{

	}

}