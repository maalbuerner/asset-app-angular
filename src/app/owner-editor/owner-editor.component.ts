import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Owner } from '../asset.model';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-owner-editor',
  templateUrl: './owner-editor.component.html',
  styleUrls: ['./owner-editor.component.css']
})
export class OwnerEditorComponent implements OnInit {
	@Input() ownerList: Array<Owner> = [];
	@Input() toUpdate: boolean;
	@Input() api: ApiService;

	@Input() titleForm: string;
	@Input() titleButton: string;
	@Input() colorButton: string;
	@Input() currentOwner: Owner;
		
	// @Output() onCreatedNewOwner: EventEmitter<Owner>;
	@Output() onUpdatedOwner: EventEmitter<Owner>;

  constructor() {
		// this.onCreatedNewOwner = new EventEmitter();
		this.onUpdatedOwner = new EventEmitter();
   }

	ngOnInit(): void {
	}

	addOwner(name: HTMLInputElement, lastname: HTMLInputElement)
	{
		if(this.toUpdate)
		{
			this.currentOwner.name = name.value;
			this.currentOwner.last_name = lastname.value;
			this.api.updateOwner(this.currentOwner, this.currentOwner.id).
			subscribe(data => {
				this.onUpdatedOwner.emit(this.currentOwner);
			});
		}
		else
		{
			// this.currentOwner = new Owner(0, name.value, lastname.value);
			var newOwner = new Owner(0, name.value, lastname.value);
			this.api.addOwner(newOwner).
			subscribe(data => {
				// this.onCreatedNewOwner.emit(new Owner(data.id, data.name, data.last_name));
				newOwner.id = data.id;				
				this.ownerList.push(newOwner);
				name.value = "";
				lastname.value = "";
			});
		}

		return false;
	}
}
