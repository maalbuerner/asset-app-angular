import { Component, Output, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-asset-navigator',
  templateUrl: './asset-navigator.component.html',
  styleUrls: ['./asset-navigator.component.css']
})
export class AssetNavigatorComponent implements OnInit {
	@Output() onNavItemSelected: EventEmitter<number>;

	/**
	* @property currentItemSelected - local state containing
	*	the currently selected (1: AssetItem, 2: CategoryItem, 3: OwnerItem)
	*/	
	currentItemSelected: number; 


  constructor() { 
 	this.onNavItemSelected = new EventEmitter();
 	this.currentItemSelected = 1;
  }

  ngOnInit(): void {
  }

  	onClickAssetNavItem(): void
	{
		this.currentItemSelected = 1;
		this.onNavItemSelected.emit(1);
	}

	onClickCategoryNavItem(): void
	{
		this.currentItemSelected = 2;
		this.onNavItemSelected.emit(2);
	}	

	onClickOwnerNavItem(): void
	{
		this.currentItemSelected = 3;
		this.onNavItemSelected.emit(3);	
	}
}
