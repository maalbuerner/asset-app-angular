import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetNavigatorComponent } from './asset-navigator.component';

describe('AssetNavigatorComponent', () => {
  let component: AssetNavigatorComponent;
  let fixture: ComponentFixture<AssetNavigatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetNavigatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetNavigatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
