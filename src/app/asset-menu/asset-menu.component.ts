import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { Asset, Category, Owner } from '../asset.model';

@Component({
  selector: 'app-asset-menu',
  templateUrl: './asset-menu.component.html',
  styleUrls: ['./asset-menu.component.css']
})
export class AssetMenuComponent implements OnInit {
	@Input() typeItem: number; /*1: Asset, 2: Category, 3: Owner*/
	@Input() idItem: number;
	@Input() idArray: number;
	@Output() onDeleteItem: EventEmitter<number[]>;
	@Output() onUpdateItem: EventEmitter<number>;
	currentPair: Array<number> = [];

	constructor() {
		this.typeItem = 1;
		this.idItem = 0;
		this.onDeleteItem = new EventEmitter();
		this.onUpdateItem = new EventEmitter();
	}

	ngOnInit(): void {
	}

	onUpdateClicked(): boolean
	{
		console.log(`typeItem: ${this.typeItem}, idItem:${this.idItem}`);
		this.onUpdateItem.emit(this.idArray);

		return false;
	}

	onDeleteClicked(): boolean
	{
		// console.log(`typeItem: ${this.typeItem}, idItem:${this.idItem}, idArray:${this.idArray}`);
		this.currentPair.push(this.idItem);
		this.currentPair.push(this.idArray);
		this.onDeleteItem.emit(this.currentPair);
		this.currentPair = [];
		return false;
	}
}
