import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Category } from '../asset.model';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-category-editor',
  templateUrl: './category-editor.component.html',
  styleUrls: ['./category-editor.component.css']
})
export class CategoryEditorComponent implements OnInit {
	@Input() toUpdate: boolean;
	@Input() api: ApiService;
	@Input() categoryList: Array<Category> = [];
	// Template info
	@Input() titleForm: string;
	@Input() titleButton: string;
	@Input() colorButton: string;
	@Input() currentCategory: Category;

	// @Output() onCreatedNewCategory: EventEmitter<Category>;
	@Output() onUpdatedCategory: EventEmitter<Category>;

	constructor() { 
		// this.onCreatedNewCategory = new EventEmitter();
		this.onUpdatedCategory = new EventEmitter();
	}

  ngOnInit(): void {
  }

	addCategory(name: HTMLInputElement): boolean
	{	
		console.log(`xxxxxxx this.currentCategory: ${this.currentCategory.name}`);

		if(this.toUpdate)
		{
			this.currentCategory.name = name.value;
			this.api.updateCategory(this.currentCategory, this.currentCategory.id).
			subscribe(data => {
				this.onUpdatedCategory.emit(this.currentCategory);
			});
		}
		else
		{
			// this.currentCategory.name = name.value;
			var newCategory = new Category(0, name.value)
			this.api.addCategory(newCategory).
			subscribe(data => {
				name.value = "";
				// this.onCreatedNewCategory.emit(new Category(data.id, data.name));
				this.categoryList.push(new Category(data.id, data.name));
			});
		}

		return false;
	}
}
