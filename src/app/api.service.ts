import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Asset, Owner, Category } from './asset.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'jwt-token',
    'Access-Control-Allow-Origin': '*'
  }),
  'responseType': 'json',
};

@Injectable({
  providedIn: 'root'
})

export class ApiService {
	private API_SERVER_URL: string = 'http://127.0.0.1:8000/api';

	constructor(private http: HttpClient) {}

	public getAssets()
	{
		return this.http.get(`${this.API_SERVER_URL}/assets`, {responseType: 'json'});
	}
	public getOwners()
	{
		return this.http.get(`${this.API_SERVER_URL}/owners`);
	}
	public getCategories()
	{
		return this.http.get(`${this.API_SERVER_URL}/categories`);
	}
	public deleteAsset(id: number)
	{
		console.log(`entro id: ${id}`);
		return this.http.delete(`${this.API_SERVER_URL}/assets/${id}`);
	}
	public deleteOwner(id: number)
	{
		return this.http.delete(`${this.API_SERVER_URL}/owners/${id}`);
	}
	public deleteCategory(id: number)
	{
		return this.http.delete(`${this.API_SERVER_URL}/categories/${id}`);
	}

	public addAsset(asset: Asset) 	{
		const rawData = {
			price : asset.price,
			epc_id : asset.epc_id,
			last_reading : asset.last_reading,
			status : asset.status,
			owner : asset.owner,
			category : asset.category
		};

		return this.http.post<any>(`${this.API_SERVER_URL}/assets/`, rawData);
	}

	public addOwner(owner: Owner)
	{
		const rawData = {
			name : owner.name,
			last_name : owner.last_name
		};

		return this.http.post<any>(`${this.API_SERVER_URL}/owners/`, rawData);
	}

	public addCategory(category: Category)
	{
		const rawData = {
			name : category.name,
		};

		return this.http.post<any>(`${this.API_SERVER_URL}/categories/`, rawData);
	}
	public updateAsset(asset: Asset, id: number) 	{
		const rawData = {
			price : asset.price,
			epc_id : asset.epc_id,
			last_reading : asset.last_reading,
			status : asset.status,
			owner : asset.owner,
			category : asset.category
		};

		return this.http.put<any>(`${this.API_SERVER_URL}/assets/${id}/`, rawData);

	}

	public updateOwner(owner: Owner, id: number)
	{
		const rawData = {
			name : owner.name,
			last_name : owner.last_name
		};

		return this.http.put<any>(`${this.API_SERVER_URL}/owners/${id}/`, rawData);
	}

	public updateCategory(category: Category, id: number)
	{
		const rawData = {
			name : category.name,
		};

		return this.http.put<any>(`${this.API_SERVER_URL}/categories/${id}/`, rawData);
	}
}
