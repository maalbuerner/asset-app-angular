import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetNavItemComponent } from './asset-nav-item.component';

describe('AssetNavItemComponent', () => {
  let component: AssetNavItemComponent;
  let fixture: ComponentFixture<AssetNavItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetNavItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetNavItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
