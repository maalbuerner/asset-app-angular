import { Component,
	EventEmitter,
	Input,
	Output,
	HostBinding,
	OnInit
} from '@angular/core';
import { Asset, Owner, Category } from '../asset.model';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-asset-list',
  templateUrl: './asset-list.component.html',
  styleUrls: ['./asset-list.component.css']
})
export class AssetListComponent implements OnInit {
	assetList: Array<Asset> = [];
	ownerList: Array<Owner> = [];
	categoryList: Array<Category> = [];
	@Input() selectedItem: number;
	@HostBinding('attr.class') cssClass = 'item';
	@Output() onMenuSelected: EventEmitter<Asset>;

	currentAsset: Asset;
	titleEditForm: string;
	titleButton: string;
	colorButton: string;
	toUpdate: boolean;
	currentArrayPos: number;

	constructor(public api: ApiService) { 
		this.onMenuSelected = new EventEmitter();
		this.titleEditForm = "Agregar artículo";
		this.titleButton = "Agregar";
		this.toUpdate = false;
		this.currentArrayPos = -1;
		this.colorButton = "ui positive";
	}

  	getData()
  	{
		this.api.getAssets()
		.subscribe(data => {
			for (const d of (data as any)) {
				this.assetList.push(
					new Asset(d.id,
						d.price,
						d.epc_id,
						d.last_reading,
						d.status,
						d.owner,
						d.category
						)
				);
			}
			console.log(this.assetList);
		});

		this.api.getOwners()
		.subscribe(data => {
			for (const d of (data as any)) {
				this.ownerList.push(
					new Owner(d.id, d.name, d.last_name)
				);
			}
			console.log(this.ownerList);
		});

		this.api.getCategories()
		.subscribe(data => {
			for (const d of (data as any)) {
				this.categoryList.push(
					new Category(d.id, d.name)
				);
			}
			console.log(this.categoryList);
		});
  	}

	ngOnInit(): void {
		this.getData();
	}

	clicked(asset: Asset): void 
	{
		console.log("entro");
		this.currentAsset = asset;
		this.onMenuSelected.emit(asset);
	}	

	isSelected(asset: Asset): boolean 
	{
		if (!asset || !this.currentAsset)
		{
			return false;
		}
		
		return asset.epc_id === this.currentAsset.epc_id;
	}	

	onRecibingNewAsset(asset: Asset): void
	{
		this.assetList.push(asset);
	}

	onUpdatedAsset(asset: Asset): void
	{
		if(this.currentArrayPos >= 0)
		{
			this.assetList[this.currentArrayPos] = asset;
			// this.currentArrayPos = -1;
		}
	}	

	onDeleteItem(pair: number[]): void
	{
		var idAsset = pair[0];
		var arrayPos = pair[1];
		this.api.deleteAsset(pair[0]).
		subscribe(data => {
			this.assetList.splice(arrayPos, 1);
		});
	}

	onUpdateItem(arrayPos: number): void
	{
		 // console.log(`arrayPos: ${arrayPos}`);
		 this.toUpdate = true;
		 //editando informacion de template
		 this.titleEditForm = "Editando categoría";
 		this.titleButton = "Modificar";
		this.colorButton = "ui primary";
		this.currentAsset = this.assetList[arrayPos];

		// console.log(`this.currentCategory: ${this.currentCategory.name}`);

		this.currentArrayPos = arrayPos; 
	}

	onAddClicked():boolean
	{
		 this.toUpdate = false;
		 //editando informacion de template
		 this.titleEditForm = "Agregar artículo";
		this.titleButton = "Agregar";
		this.colorButton = "ui positive";
		var ownerIdTemp = 0;
		var categoryIdTemp = 0;
		if(this.ownerList.length>0)
			ownerIdTemp = this.ownerList[0].id;
		if(this.categoryList.length>0)
			categoryIdTemp = this.categoryList[0].id;



		this.currentAsset = new Asset(0, 0, "", "", "", ownerIdTemp, categoryIdTemp);
		this.currentArrayPos = -1; 

		return false;
	}

	getOwnerNameById(id: number):string
	{
		var fullname = "";

		for (var i = 0; i < this.ownerList.length; i++) {
			if(this.ownerList[i].id == id)
			{
				fullname = this.ownerList[i].name+" "+this.ownerList[i].last_name;
				break;
			}
		}
		return  fullname;
	}
	getCategoryNameById(id: number):string
	{
		var fullname = "";

		for (var i = 0; i < this.categoryList.length; i++) {
			if(this.categoryList[i].id == id)
			{
				fullname = this.categoryList[i].name;
				break;
			}
		}
		return  fullname;
	}
	getStatusRepresentation(status: string)
	{
		if( status == "miss")
			return "Faltante";
		if( status == "none")
			return "Buscando";
		if( status == "readed")
			return "Presente";

		return "";
	}
}
