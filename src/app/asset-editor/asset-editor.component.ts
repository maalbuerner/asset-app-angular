import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Asset, Owner, Category } from '../asset.model';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-asset-editor',
  templateUrl: './asset-editor.component.html',
  styleUrls: ['./asset-editor.component.css']
})
export class AssetEditorComponent implements OnInit {
	@Input() assetList: Asset[];
	@Input() ownerList: Owner[];
	@Input() categoryList: Category[];
	@Input() currentAsset: Asset;
	@Input() api: ApiService;
	@Output() onCreatedNewAsset: EventEmitter<Asset>;
	@Output() onUpdatedAsset: EventEmitter<Asset>;

		// Template info
	@Input() toUpdate: boolean;
	@Input() titleForm: string;
	@Input() titleButton: string;
	@Input() colorButton: string;
	@Input() currentCategory: Category;


	constructor() {
		this.onCreatedNewAsset = new EventEmitter();				
		this.onUpdatedAsset = new EventEmitter();
		this.colorButton = "positive";				
	}

	ngOnInit(): void {
	}

	addAsset(price: HTMLInputElement, epc_id: HTMLInputElement, status: HTMLSelectElement, categoryid: HTMLSelectElement, ownerid: HTMLSelectElement): boolean
	{
		console.log(status);

		// if(this.toUpdate)
		// {
		// 	this.currentAsset.price = parseInt(price.value);
		// 	this.currentAsset.epc_id = epc_id.value;
		// 	this.currentAsset.last_reading = "2020-05-06 00:00:00";
		// 	this.currentAsset.status = status.value;
		// 	this.currentAsset.owner = parseInt(ownerid.value);
		// 	this.currentAsset.category = parseInt(categoryid.value)
		// 	this.api.updateAsset(this.currentAsset, this.currentAsset.id).
		// 	subscribe(data => {
		// 		this.onUpdatedAsset.emit(this.currentAsset);
		// 	});
		// }
		// else
		// {
		// 	var newAsset = new Asset(0, parseInt(price.value), epc_id.value, "2020-05-06 00:00:00",
		// 		status.value, parseInt(ownerid.value), parseInt(categoryid.value));
		// 	this.api.addAsset(newAsset).
		// 	subscribe(data => {
		// 		// this.onCreatedNewAsset.emit(new Asset(data.id, data.price, data.epc_id, data.last_reading,
		// 		// 			data.status, data.owner, data.category));
		// 		this.assetList.push(new Asset(data.id, data.price, data.epc_id, data.last_reading,
		// 					data.status, data.owner, data.category));
		// 		price.value = "";
		// 		epc_id.value = "";
		// 		status.value = "";
		// 		categoryid.selectedIndex = 0;
		// 		ownerid.selectedIndex = 0;
		// 	});
		// }
		return false;
	}  
}
