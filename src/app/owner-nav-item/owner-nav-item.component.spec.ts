import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnerNavItemComponent } from './owner-nav-item.component';

describe('OwnerNavItemComponent', () => {
  let component: OwnerNavItemComponent;
  let fixture: ComponentFixture<OwnerNavItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OwnerNavItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnerNavItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
