import { Component, Output, EventEmitter } from '@angular/core';
import { Asset, Owner, Category } from './asset.model';
import {
RouterModule,
Routes
} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	assetList: Array<Asset> = [];
	categoryList: Array<Category> = [];
	ownerList: Array<Owner> = [];r
	data: any = [];
	selectedItemNav: number = 1;

  	constructor()
  	{
  	// 		this.assetList = [
			// 	new Asset(
			// 		1,
			// 		34.99,
			// 		'#234734234',
			// 		"2020-05-06 00:00:00",
			// 		'miss',
			// 		3,
			// 		3),
			// 	new Asset(
			// 		2,
			// 		222.99,
			// 		'#23473232423',
			// 		"2020-05-06 00:00:00",
			// 		'none',
			// 		4,
			// 		6),
			// 	new Asset(
			// 		3,
			// 		0.99,
			// 		'#23407897856',
			// 		"2020-05-06 00:00:00",
			// 		'modeliss',
			// 		2,
			// 		5),
			// ];

  	}

  	// getData()
  	// {
		// this.api.getAssets()
		// .subscribe(data => {
		// 	for (const d of (data as any)) {
		// 		this.assetList.push(
		// 			new Asset(d.id,
		// 				d.price,
		// 				d.epc_id,
		// 				d.last_reading,
		// 				d.status,
		// 				d.owner,
		// 				d.category
		// 				)
		// 		);
		// 	}
		// 	console.log(this.assetList);
		// });

		// this.api.getOwners()
		// .subscribe(data => {
		// 	for (const d of (data as any)) {
		// 		this.ownerList.push(
		// 			new Owner(d.id, d.name, d.last_name)
		// 		);
		// 	}
		// 	console.log(this.ownerList);
		// });

		// this.api.getCategories()
		// .subscribe(data => {
		// 	for (const d of (data as any)) {
		// 		this.categoryList.push(
		// 			new Category(d.id, d.name)
		// 		);
		// 	}
		// 	console.log(this.categoryList);
		// });
  // 	}

  	// deleteAsset(id: number): void
  	// {
  	// 	this.api.deleteAsset(id);
  	// }

  	// deleteOwner(id: number): void
  	// {
  	// 	this.api.deleteOwner(id);
  	// }

  	// deleteCategory(id: number): void
  	// {
  	// 	this.api.deleteCategory(id);
  	// }

  	// addAsset(asset: Asset): void
  	// {
  	// 	this.api.addAsset(asset);
  	// }

  	// addOwner(owner: Owner): void
  	// {
  	// 	this.api.addOwner(owner);
  	// }

  	// addCategory(category: Category): void
  	// {
  	// 	this.api.addCategory(category);
  	// }

  	// updateAsset(asset: Asset, id: number): void
  	// {
  	// 	this.api.updateAsset(asset, id);
  	// }

  	// updateOwner(owner: Owner, id: number): void
  	// {
  	// 	this.api.updateOwner(owner, id);
  	// }

  	// updateCategory(category: Category, id: number): void
  	// {
  	// 	this.api.updateCategory(category, id);
  	// }


	ngOnInit(): void {
		// this.getData();		
		 // this.deleteAsset(3); 
		 // this.deleteCategory(4); 
		 // this.deleteOwner(2); 
		 // this.addAsset(this.assetList[0]);
		 // this.updateCategory(new Category(1, 'Ventilador'), 7);
	}

	onNavItemSelected(item: number): void
	{
		console.log("item: " + item);
		this.selectedItemNav = item;
	}
}
