import { Component, OnInit, Input } from '@angular/core';
import { Owner } from '../asset.model';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-owner-list',
  templateUrl: './owner-list.component.html',
  styleUrls: ['./owner-list.component.css']
})
export class OwnerListComponent implements OnInit {
	ownerList: Array<Owner> = [];
	currentArrayPos: number;
	toUpdate: boolean;
	currentOwner: Owner;

	// informacion de edicion
	titleEditForm: string; 
	titleButton: string;
	colorButton: string;

	constructor(public api: ApiService) { 
		this.currentArrayPos = -1;
		this.toUpdate = false;
		this.titleEditForm = "Agregar responsable";
		this.titleButton = "Agregar";
		this.colorButton = "positive";
	}

	getData()
	{
		this.api.getOwners()
		.subscribe(data => {
			for (const d of (data as any)) {
				this.ownerList.push(
					new Owner(d.id, d.name, d.last_name)
				);
			}
			console.log(this.ownerList);
		});
	}

	ngOnInit(): void {
		this.getData();
	}

	clicked(owner: Owner): void 
	{
		this.currentOwner = owner;
	}	

	onRecibingNewOwner(owner: Owner): void
	{
		this.currentOwner = owner;
		this.ownerList.push(owner);
	}

	onDeleteItem(pair: number[]): void
	{
		var idOwner = pair[0];
		var arrayPos = pair[1];
		this.api.deleteOwner(idOwner).
		subscribe(data => {
			this.ownerList.splice(arrayPos, 1);
		});
	}

	onUpdateItem(arrayPos: number): void
	{
		 // console.log(`arrayPos: ${arrayPos}`);
		 this.toUpdate = true;
		 //editando informacion de template
		 this.titleEditForm = "Editando responsable";
 		this.titleButton = "Modificar";
 		this.colorButton = "primary";
		this.currentOwner = this.ownerList[arrayPos];

		// console.log(`this.currentCategory: ${this.currentCategory.name}`);

		this.currentArrayPos = arrayPos; 
	}

	onUpdatedOwner(owner: Owner): void
	{
		if(this.currentArrayPos >= 0)
		{
			this.ownerList[this.currentArrayPos] = owner;
			// this.currentArrayPos = -1;
		}
	}

	onAddClicked():boolean
	{
		 this.toUpdate = false;
		 //editando informacion de template
		 this.titleEditForm = "Agregar responsable";
		this.titleButton = "Agregar";
		this.colorButton = "positive";
		this.currentOwner = new Owner(0, "", "");
		this.currentArrayPos = -1; 

		return false;
	}
}
