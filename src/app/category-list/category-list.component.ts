import { Component, OnInit, Input } from '@angular/core';
import { Category } from '../asset.model';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {
	categoryList: Array<Category> = [];
	currentCategory: Category;
	currentArrayPos: number;
	toUpdate: boolean;

	// informacion de edicion
	titleEditForm: string; 
	titleButton: string;
	colorButton: string;

	constructor(public api: ApiService) {
		this.currentCategory = new Category(0,"");
		this.currentArrayPos = -1;
		this.toUpdate = false;
		this.titleEditForm = "Agregar categoría";
		this.titleButton = "Agregar";
		this.colorButton = "positive";
	}

  	getData()
  	{
		this.api.getCategories()
		.subscribe(data => {
			for (const d of (data as any)) {
				this.categoryList.push(
					new Category(d.id, d.name)
				);
			}
			// console.log("Loaded categories: \n"+this.categoryList);
		});
  	}

	ngOnInit(): void {
		this.getData();
	}

	// clicked(category: Category): void 
	// {
	// 	this.currentCategory = category;
	// }

	// onRecibingNewCategory(category: Category): void
	// {
	// 	this.categoryList.push(category);
	// }

	onDeleteItem(pair: number[]): void
	{	
		var idCategory = pair[0];
		var arrayPos = pair[1];
		this.api.deleteCategory(idCategory).
		subscribe(data => {
			this.categoryList.splice(arrayPos, 1);
		});
	}

	onUpdateItem(arrayPos: number): void
	{
		 // console.log(`arrayPos: ${arrayPos}`);
		 this.toUpdate = true;
		 //editando informacion de template
		 this.titleEditForm = "Editando categoría";
 		this.titleButton = "Modificar";
 		this.colorButton = "primary";
		this.currentCategory = this.categoryList[arrayPos];

		// console.log(`VVVVVVV this.currentCategory: ${this.currentCategory.name}`);

		this.currentArrayPos = arrayPos; 
	}

	onUpdatedCategory(category: Category): void
	{
		if(this.currentArrayPos >= 0)
		{
			this.categoryList[this.currentArrayPos] = category;
			this.currentArrayPos = -1;
		}
	}

	onAddClicked():boolean
	{
		 this.toUpdate = false;
		 //editando informacion de template
		 this.titleEditForm = "Agregar categoría";
		this.titleButton = "Agregar";
		this.colorButton = "positive";
		this.currentCategory = new Category(0, "");
		this.currentArrayPos = -1; 

		return false;
	}
}
